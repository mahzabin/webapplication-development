<?php
    echo ' this is a simple string';
    echo 'you can also have embedded newlones in strings this way as it is okay to do';
    //outputs:Arnold once said: "i'll be back"
    echo 'Arnold once said: "I\'ll be back"';
    //output: you deleted c:\*.* ?
    echo 'you deleted c:\\*.*?';
    // output : you deleted c:\*.*?
    echo 'you deleted c: \*.*?';
    // output: variables do not $expand $either
    echo 'variables do not $expand $either';
    ?>
